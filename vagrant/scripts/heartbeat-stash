#!/bin/sh
#
# Description:  Manages a Stash Server as an OCF High-Availability
#               resource under Heartbeat/LinuxHA control
#
# License:      BSD 3
#
# Copyright (c) 2013, Atlassian
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this list 
# of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice, this list 
# of conditions and the following disclaimer in the documentation and/or other 
# materials provided with the distribution.
#
# Neither the name of the Atlassian nor the names of its contributors may be used to 
# endorse or promote products derived from this software without specific prior written 
# permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
# SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
# TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY 
# WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#######################################################################
# OCF parameters:
#   OCF_RESKEY_stash_home - The directory where Stash stores its data
#   OCF_RESKEY_stash_inst - The directory where Stash is installed.
#   OCF_RESKEY_stash_user - The user to start Stash as. Should *not* be root.
#   OCF_RESKEY_statusurl  - URL for state confirmation. Default is
#                           http://127.0.0.1:7990/status
#   OCF_RESKEY_java_home  - Home directory of Java. Default is none
#   OCF_RESKEY_log_file   - Log file used to log start/stop feedback
###############################################################################

# Initialization
: ${OCF_FUNCTIONS_DIR=${OCF_ROOT}/lib/heartbeat}
. ${OCF_FUNCTIONS_DIR}/ocf-shellfuncs

############################################################################
# Usage
usage() 
{
    cat <<-END
usage: $0 action

action:
        start   start Stash

        stop    stop Stash

        status  return the status of Stash, up or down

        monitor  return TRUE if Stash appears to be working.
                 You have to have installed $WGETNAME for this to work.

        meta-data       show meta data message

        validate-all    validate the instance parameters
END
}

############################################################################
# Check Stash service availability
############################################################################

isavailable_stash()
{
    if ! have_binary $WGET; then
       ocf_log err "Monitoring is not supported by $OCF_RESOURCE_INSTANCE"
       ocf_log info "Please make sure that wget is available"
       return $OCF_ERR_CONFIGURED
    fi
    $WGET -t 1 --connect-timeout=1 --read-timeout=3 -q -O - $STASH_STATUSURL | grep "FIRST_RUN\|RUNNING" >/dev/null 2>&1
}

############################################################################
#
############################################################################

isalive_stash()
{
    pgrep -f "${STASH_PS_SEARCH_STRING}" > /dev/null
    if [ $? != 0 ]; then
       return $OCF_NOT_RUNNING
    fi

    return $OCF_SUCCESS
}

############################################################################
# Check Stash process and service availability
############################################################################

monitor_stash()
{
    isalive_stash || return $OCF_NOT_RUNNING
    isavailable_stash 
  
    return $?
}

############################################################################
# Start Stash
############################################################################

start_stash()
{
    cd "$STASH_INST/bin"

    # check whether Stash is already running
    if isavailable_stash; then
       return $OCF_SUCCESS
    fi

    # not running, start Stash
    echo "" >> "$STASH_LOG"
    echo "###############################################################" >> "$STASH_LOG"
    echo "`date "+%Y/%m/%d %T"`: start [stash_inst=$STASH_INST $stash_user=$STASH_USER]" >> "$STASH_LOG"
    echo "###############################################################" >> "$STASH_LOG"
    echo "" >> "$STASH_LOG"

    su -c "$STASH_INST/bin/start-stash.sh" "$STASH_USER" >> "$STASH_LOG" 2>&1 &
  
    # wait for Stash to be up and running
    start_timeout=240
    if [ -n "$OCF_RESKEY_CRM_meta_timeout" ]; then
        start_timeout=$(($OCF_RESKEY_CRM_meta_timeout/1000))
    fi
    
    now=`date "+%s"`
    timeout=$(($start_timeout+$now))
    while [ $now -lt $timeout ]
    do
        if isavailable_stash; then
            echo "`date "+%Y/%m/%d %T"`: Startup complete" >> "$STASH_LOG"
            ocf_log info "start_stash: startup complete"
            return $OCF_SUCCESS
        fi
        sleep 2
        ocf_log debug "Stash hasn't started yet. Waiting..."
        now=`date "+%s"`
    done

    ocf_log info "Stash failed to start after ${start_timeout}s. Killing the process..."
    echo "`date "+%Y/%m/%d %T"`: Stash failed to start. Killing the process" >> "$STASH_LOG"
    pkill -KILL -f "${STASH_PS_SEARCH_STRING}"
    return $OCF_ERR_GENERIC
}

############################################################################
# Stop Stash
############################################################################

stop_stash()
{
    echo "" >> "$STASH_LOG"
    echo "###############################################################" >> "$STASH_LOG"
    echo "`date "+%Y/%m/%d %T"`: stop [stash_inst=$STASH_INST $stash_user=$STASH_USER]" >> "$STASH_LOG"
    echo "###############################################################" >> "$STASH_LOG"
    echo "" >> "$STASH_LOG"

    su -c "$STASH_INST/bin/stop-stash.sh" "$STASH_USER" >> "$STASH_LOG" 2>&1 &

    stop_timeout=15
    if [ -n "$OCF_RESKEY_CRM_meta_timeout" ]; then
        stop_timeout=$(($OCF_RESKEY_CRM_meta_timeout/1000))
    fi
    count=0
    while [ $count -lt $stop_timeout ]
    do
        isalive_stash
        if [ $? = $OCF_NOT_RUNNING ]; then
            echo "`date "+%Y/%m/%d %T"`: Stash stopped" >> "$STASH_LOG"
            return $OCF_SUCCESS
        fi
        sleep 1
        count=`expr $count + 1`
        ocf_log debug "Stash hasn't stopped yet. Waiting..."
    done

    ocf_log info "Stash failed to stop after ${stop_timeout}s. Killing the process..."
    echo "`date "+%Y/%m/%d %T"`: Stash failed to stop. Killing the process" >> "$STASH_LOG"
  
    pkill -KILL -f "${STASH_PS_SEARCH_STRING}"

    return $OCF_SUCCESS
}

status_stash()
{
    return $OCF_SUCCESS
}


metadata_stash()
{
    cat <<END
<?xml version="1.0"?>
<!DOCTYPE resource-agent SYSTEM "ra-api-1.dtd">
<resource-agent name="stash">
<version>1.0</version>

<longdesc lang="en">
  Resource script for Stash. It manages a Stash instance as a cluster resource.
</longdesc>
<shortdesc lang="en">Manages a Stash instance</shortdesc>

<parameters>

  <parameter name="log_file" unique="1">
    <longdesc lang="en">
       Log file, used during start and stop operations.
    </longdesc>
    <shortdesc>Log file</shortdesc>
    <content type="string" default="" />
  </parameter>

  <parameter name="stash_user" unique="0" required="1">
    <longdesc lang="en">
      The user who runs Stash.
    </longdesc>
    <shortdesc>The user who tuns Stash</shortdesc>
    <content type="string" default="" />
  </parameter>

  <parameter name="statusurl" unique="0">
    <longdesc lang="en">
      URL for state confirmation.
    </longdesc>
    <shortdesc>URL for state confirmation</shortdesc>
    <content type="string" default="" />
  </parameter>

  <parameter name="stash_inst" unique="1" required="1">
    <longdesc lang="en">
      Installation directory of Stash
    </longdesc>
    <shortdesc>Installation directory of Stash</shortdesc>
    <content type="string" default="" />
  </parameter>

  <parameter name="stash_home" unique="1" required="0">
    <longdesc lang="en">
      Stash Home directory
    </longdesc>
    <shortdesc>Home directory of Stash</shortdesc>
    <content type="string" default="" />
  </parameter>

</parameters>

<actions>
  <action name="start" timeout="180s" />
  <action name="stop" timeout="120s" />
  <action name="status" timeout="10s" />
  <action name="monitor" depth="0" timeout="30s" interval="30s" />
  <action name="meta-data" timeout="5s" />
  <action name="validate-all" timeout="5s"/>
</actions>

</resource-agent>
END
    return $OCF_SUCCESS
}

validate_all_stash()
{
    ocf_log info "validate_all_stash"
    return $OCF_SUCCESS
}

#-------------------
# Stash Resource Agent variables
#-------------------

STASH_INST="${OCF_RESKEY_stash_inst}"
STASH_HOME="${OCF_RESKEY_stash_home}"
STASH_USER="${OCF_RESKEY_stash_user}"
STASH_STATUSURL="${OCF_RESKEY_statusurl-http://127.0.0.1:7990/status}"
STASH_PS_SEARCH_STRING="Dcatalina.home=${STASH_INST}"
STASH_LOG="${OCF_RESKEY_log_file-/var/log/stash-ha.log}"

export STASH_HOME="$STASH_HOME"

#
# ------------------
# the main script
# ------------------
# 

COMMAND=$1

case "$COMMAND" in
    start)
        ocf_log debug  "Enter Stash start"
        start_stash
        func_status=$?
        ocf_log debug  "Leave Stash start $func_status"
        exit $func_status
        ;;
    stop)
        ocf_log debug  "Enter Stash stop"
        stop_stash
        func_status=$?
        ocf_log debug  "Leave Stash stop $func_status"
        exit $func_status
        ;;
    status)
        ocf_log debug  "Enter Stash status"
        status_stash
        func_status=$?
        ocf_log debug  "Leave Stash status $func_status"             
        exit $func_status
        ;;
    monitor)
        ocf_log debug  "Enter Stash monitor"
        monitor_stash
        func_status=$?
        ocf_log debug  "Leave Stash monitor $func_status"
        exit $func_status
        ;;
    meta-data)
        metadata_stash
        exit $?
        ;;
    validate-all)
        validate_all_stash
        exit $?
        ;;
    usage|help)
        usage
        exit $OCF_SUCCESS
        ;;
    *)
       usage
       exit $OCF_ERR_UNIMPLEMENTED
       ;;
esac
