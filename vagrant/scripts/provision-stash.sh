#!/bin/bash

STASH_VERSION="2.10.1"

#$1 contains the node-id
HOSTNAME="stash${1}.cluster"
IP="192.168.56.1${1}"

echo "Provisioning the cluster as user `whoami`"

# add hosts entries for all nodes in the cluster
sed -i.orig "s/^\(.*\)stash${1}\.cluster stash${1}\(.*\)/\1\2/" /etc/hosts
cat >> /etc/hosts <<END

192.168.56.11  stash1      stash1.cluster
192.168.56.12  stash2      stash2.cluster
192.168.56.99  stash       stash.cluster
END

# initialize the drbd disk
cat > /etc/drbd.conf <<EOF
global {
    # do not participate in online usage survey
    usage-count no;
}
 
resource data {
 
    # write IO is reported as completed if it has reached both local
    # and remote disk
    protocol C;
 
    net {
        # allow both nodes to be the primary
        allow-two-primaries;
        # set up peer authentication
        cram-hmac-alg sha1;
        shared-secret "t0ps3cr3tp@ssw0rd";
        # default value 32 - increase as required
        max-buffers 512;
        # highest number of data blocks between two write barriers
        max-epoch-size 512;
        # size of the TCP socket send buffer - can tweak or set to 0 to
        # allow kernel to autotune
        sndbuf-size 0;
    }
 
    startup {
        # wait for connection timeout - boot process blocked
        # until DRBD resources are connected
        wfc-timeout 10;
        # WFC timeout if peer was outdated
        outdated-wfc-timeout 10;
        # WFC timeout if this node was in a degraded cluster (i.e. only had one
        # node left)
        degr-wfc-timeout 10;
        # on startup, switch the resource to the primary role
        become-primary-on both;
    }
 
    disk {
        # the next two are for safety - detach on I/O error
        # and set up fencing - resource-only will attempt to
        # reach the other node and fence via the fence-peer 
        # handler
        on-io-error detach;
        fencing dont-care; # resource-only to fence a node that becomes unavailable 
        # no-disk-flushes; # if we had battery-backed RAID
        # no-md-flushes; # if we had battery-backed RAID
        # ramp up the resync rate
        resync-rate 100M;
    }
    handlers {
        # specify the two fencing handlers
        # see: http://www.drbd.org/users-guide-8.4/s-pacemaker-fencing.html
        fence-peer "/usr/lib/drbd/crm-fence-peer.sh";
        after-resync-target "/usr/lib/drbd/crm-unfence-peer.sh";
    }
    on stash1.cluster {
        # DRBD device
        device /dev/drbd0;
        # backing store device
        disk /dev/sda2;
        # IP address of node, and port to listen on
        address 192.168.56.11:7789;
        # use internal meta data (don't create a filesystem before 
        # you create metadata!)
        meta-disk internal;
    }
    on stash2.cluster {
        # DRBD device
        device /dev/drbd0;
        # backing store device
        disk /dev/sda2;
        # IP address of node, and port to listen on
        address 192.168.56.12:7789;
        # use internal meta data (don't create a filesystem before 
        # you create metadata!)
        meta-disk internal;
    }
}
EOF

# Shutdown the firewall...
chkconfig iptables off
service iptables stop

# Set up DRBD volume
echo "Wiping /data disk in preparation of DRBD configuration"
umount /data
# wipe the disk
dd if=/dev/zero of=/dev/sda2 count=2048 bs=1024k
# create the DRBD device (/dev/drbd0)
drbdadm create-md data


# Disable quorum (not relevant for 2 node cluster)
sed -i.orig "s/.*CMAN_QUORUM_TIMEOUT=.*/CMAN_QUORUM_TIMEOUT=0/g" /etc/sysconfig/cman

# configure the cluster
ccs -f /etc/cluster/cluster.conf --createcluster atlascluster
ccs -f /etc/cluster/cluster.conf --addnode stash1.cluster
ccs -f /etc/cluster/cluster.conf --addnode stash2.cluster
ccs -f /etc/cluster/cluster.conf --addfencedev pcmk agent=fence_pcmk
ccs -f /etc/cluster/cluster.conf --addmethod pcmk-redirect stash1.cluster
ccs -f /etc/cluster/cluster.conf --addmethod pcmk-redirect stash2.cluster
ccs -f /etc/cluster/cluster.conf --addfenceinst pcmk stash1.cluster pcmk-redirect port=stash1.cluster
ccs -f /etc/cluster/cluster.conf --addfenceinst pcmk stash2.cluster pcmk-redirect port=stash2.cluster

chkconfig drbd on
service drbd start 2> /dev/null

# The next operations should only be performed on one node
if [ "$1" = "1" ]; then
  echo "Creating GFS2 volume"
  drbdadm --force primary data
  echo "y"|mkfs.gfs2 -q -p lock_dlm -j 2 -t atlascluster:data /dev/drbd0 > /dev/null 2> /dev/null
fi

# Wait for the DRBD disk sync to complete
diskSyncing () {
   cat /proc/drbd | grep -q "ds:UpToDate"
}

diskSyncing
echo -n "Waiting for DRBD sync to complete "
while [ $? -ne 0 ]
do
   sleep 5;
   echo -en "\b.."
   diskSyncing
done
echo ""
echo "DRBD sync is complete"


# configure the cluster to automatically start up on boot
chkconfig pacemaker on
service pacemaker start

# alter fstab entry for /data
echo "Mounting clustered filesystem (/data)"
sed -i.orig "s/\(.*\/data.*\)ext4\(.*\)/\/dev\/drbd0\\t\/data\\tgfs2\\tdefaults\\t0 0/" /etc/fstab

if [ "$1" = "1" ]; then
  # Mounting /data on node1 will hang until node2 becomes available. Apparently a cluster of 1 is not considered a cluster
  nohup mount /data &
else
  mount /data
  mkdir -p /data/stash/home
  chown -R stash:stash /data
fi

# The following assumes that a Stash distribution is unpacked in the stash-ha/stash-inst directory
cat > /etc/profile.d/stash.sh <<EOF
export STASH_HOME="/data/stash/home"
EOF

cat > /etc/profile.d/stash.csh <<EOF
setenv STASH_HOME "/data/stash/home"
EOF

cp /vagrant/scripts/heartbeat-stash /usr/lib/ocf/resource.d/heartbeat/stash
chmod a+x /usr/lib/ocf/resource.d/heartbeat/stash

# Download and unpack Stash in /vagrant if not yet present
if [ ! -e /opt/stash/current ]; then
   if [ ! -e /opt/stash/atlassian-stash-$STASH_VERSION ]; then
      pushd /opt/stash
      echo "Downloading Stash $STASH_VERSION"
      wget -q http://www.atlassian.com/software/stash/downloads/binary/atlassian-stash-$STASH_VERSION.tar.gz
      echo "Unpacking atlassian-stash-$STASH_VERSION.tar.gz"
      tar xvzf atlassian-stash-$STASH_VERSION.tar.gz > /dev/null 2> /dev/null
      popd
   fi

   ln -s /opt/stash/atlassian-stash-$STASH_VERSION /opt/stash/current
fi


if [ "$1" = "2" ]; then
  echo "Configuring cluster"
  pcs property set stonith-enabled=false
  pcs property set no-quorum-policy=ignore
  pcs resource create ha_ip ocf:heartbeat:IPaddr2 ip=192.168.56.99 cidr_netmask=32 op monitor interval=30s
  pcs resource create stash_res ocf:heartbeat:stash params stash_user=stash stash_home=/data/stash/home stash_inst=/opt/stash/current op monitor interval=15s op start timeout=240s
  # have Stash fail over to the other node on the first failure
  pcs resource meta stash_res migration-threshold=1
  # configure the high available IP to follow Stash
  pcs constraint colocation add ha_ip with stash_res INFINITY
fi

echo "All done"

