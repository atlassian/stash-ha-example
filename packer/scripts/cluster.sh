# Cluster software
yum -y install corosync pacemaker python-dateutil redhat-rpm-config
yum -y install perl-ExtUtils-MakeMaker gettext-devel expat-devel curl-devel
rpm --import http://elrepo.org/RPM-GPG-KEY-elrepo.org
rpm -Uvh http://elrepo.org/elrepo-release-6-5.el6.elrepo.noarch.rpm
yum -y install drbd84-utils kmod-drbd84 
chkconfig drbd off
yum install -y cman gfs2-utils rgmanager pcs ccs resource-agents

# Editors
yum -y install nano emacs 

# Stash specific dependencies
yum -y install java-1.7.0-openjdk-devel mysql-connector-java 

# Add stash user
useradd -u 1033 stash
