# Base install

sed -i "s/^.*requiretty/#Defaults requiretty/" /etc/sudoers

yum -y install gcc make gcc-c++ kernel-devel-`uname -r` zlib-devel openssl-devel readline-devel sqlite-devel perl wget dkms ruby ruby-devel kernel-devel kernel-headers

# Install Git
yum -y install curl-devel expat-devel gettext-devel perl-ExtUtils-Embed
cd /tmp
wget http://git-core.googlecode.com/files/git-1.8.3.4.tar.gz
tar xvzf git-1.8.3.4.tar.gz
cd git-1.8.3.4
./configure
make prefix=/usr all
make prefix=/usr install
